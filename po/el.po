# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# ΔΗΜΗΤΡΗΣ ΣΠΙΓΓΟΣ <dmtrs32@gmail.com>, 2014
msgid ""
msgstr ""
"Project-Id-Version: realmd\n"
"Report-Msgid-Bugs-To: https://gitlab.freedesktop.org/realmd/realmd/-/issues\n"
"POT-Creation-Date: 2022-09-29 13:14+0200\n"
"PO-Revision-Date: 2017-09-19 19:43+0000\n"
"Last-Translator: Stef Walter <stefw@gnome.org>\n"
"Language-Team: Greek (http://www.transifex.com/freedesktop/realmd/language/"
"el/)\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: service/org.freedesktop.realmd.policy.in:14
msgid "Discover realm"
msgstr "Ανακαλύψτε το πεδίο"

#: service/org.freedesktop.realmd.policy.in:15
msgid "Authentication is required to discover a kerberos realm"
msgstr "Απαιτείται πιστοποίηση για να ανακαλύψετε ένα πεδίο kerberos"

#: service/org.freedesktop.realmd.policy.in:24
msgid "Join machine to realm"
msgstr "Συνδέστε το μηχάνημα στο πεδίο"

#: service/org.freedesktop.realmd.policy.in:25
msgid "Authentication is required to join this machine to a realm or domain"
msgstr ""
"Απαιτείται πιστοποίηση για να συνδέσετε αυτό το μηχάνημα σε ένα πεδίο ή τομέα"

#: service/org.freedesktop.realmd.policy.in:38
msgid "Remove machine from realm"
msgstr "Αφαίρεση μηχανήματος από το πεδίο"

#: service/org.freedesktop.realmd.policy.in:39
msgid ""
"Authentication is required to remove this computer from a realm or domain."
msgstr ""
"Απαιτείται πιστοποίηση για να αφαιρέσετε αυτόν τον υπολογιστή από ένα πεδίο "
"ή τομέα."

#: service/org.freedesktop.realmd.policy.in:48
msgid "Change login policy"
msgstr "Αλλαγή πολιτικής σύνδεσης"

#: service/org.freedesktop.realmd.policy.in:49
msgid ""
"Authentication is required to change the policy of who can log in on this "
"computer."
msgstr ""
"Απαιτείται πιστοποίηση για να αλλάξετε την πολιτική δυνατότητας σύνδεσης σε "
"αυτόν τον υπολογιστή."

#: service/realm-command.c:347
#, c-format
msgid "Process was terminated with signal: %d"
msgstr "Η διεργασία τελείωσε με το σήμα: %d"

#: service/realm-command.c:400 service/realm-ldap.c:424
msgid "The operation was cancelled"
msgstr "Η λειτουργία ακυρώθηκε"

#: service/realm-command.c:577
#, c-format
msgid "Configured command not found: %s"
msgstr "Δεν βρέθηκε η ρυθμισμένη εντολή: %s"

#: service/realm-command.c:582
#, c-format
msgid "Skipped command: %s"
msgstr "Παραλείφθηκε η εντολή: %s"

#: service/realm-command.c:588
#, c-format
msgid "Configured command invalid: %s"
msgstr "Η ρυθμισμένη εντολή είναι άκυρη: %s"

#: service/realm-disco-mscldap.c:198
msgid "Received invalid or unsupported Netlogon data from server"
msgstr "Ελήφθησαν άκυρα ή ανυποστήρικτα δεδομένα Netlogon από τον διακομιστή"

#: service/realm-disco-mscldap.c:346
msgid "LDAP on this system does not support UDP connections"
msgstr "Το LDAP σε αυτό το σύστημα δεν υποστηρίζει συνδέσεις UDP"

#: service/realm-disco-mscldap.c:354 service/realm-disco-rootdse.c:480
msgid "Failed to setup LDAP connection"
msgstr ""

#: service/realm-example.c:82 service/realm-samba.c:293
#: service/realm-sssd-ad.c:362 service/realm-sssd-ipa.c:304
#, c-format
msgid "Unsupported or unknown membership software '%s'"
msgstr "Ανυποστήρικτο ή άγνωστο λογισμικό μέλους '%s'"

#: service/realm-example.c:170 service/realm-samba.c:334
msgid "Already joined to a domain"
msgstr "Έχετε ήδη συνδεθεί με έναν τομέα"

#: service/realm-example.c:178 service/realm-example.c:271
msgid "Admin name or password is not valid"
msgstr "Το όνομα διαχειριστή ή ο κωδικός πρόσβασης δεν είναι έγκυρος"

#: service/realm-example.c:242 service/realm-samba.c:475
#: service/realm-sssd-ad.c:584
msgid "Not currently joined to this domain"
msgstr "Δεν είσαστε συνδεμένοι τώρα σε αυτόν τον τομέα"

#: service/realm-example.c:301
msgid "Need credentials for leaving this domain"
msgstr "Χρειάζονται διαπιστευτήρια για να αφήσετε αυτόν τον τομέα"

#: service/realm-ini-config.c:736
#, c-format
msgid "Couldn't write out config: %s"
msgstr "Αδύνατη η εγγραφή διαμόρφωσης: %s"

#: service/realm-invocation.c:536
msgid "Not authorized to perform this action"
msgstr "Δεν είστε εξουσιοδοτημένος να εκτελέσετε αυτήν την ενέργεια."

#: service/realm-kerberos.c:130 service/realm-kerberos.c:208
#: service/realm-provider.c:158
msgid "Operation was cancelled."
msgstr "Η λειτουργία ακυρώθηκε."

#: service/realm-kerberos.c:135
msgid "Failed to enroll machine in realm. See diagnostics."
msgstr "Αποτυχία εγγραφής του μηχανήματος στο πεδίο. Δείτε τα διαγνωστικά."

#: service/realm-kerberos.c:213
msgid "Failed to unenroll machine from domain. See diagnostics."
msgstr ""
"Αποτυχία αφαίρεσης εγγραφής του μηχανήματος από τον τομέα. Δείτε τα "
"διαγνωστικά."

#: service/realm-kerberos.c:273
msgid "Joining this realm without credentials is not supported"
msgstr "Η σύνδεση σε αυτό το πεδίο χωρίς διαπιστευτήρια δεν υποστηρίζεται"

#: service/realm-kerberos.c:274
msgid "Leaving this realm without credentials is not supported"
msgstr "Η απομάκρυνση αυτού του πεδίου χωρίς διαπιστευτήρια δεν υποστηρίζεται"

#: service/realm-kerberos.c:277
msgid "Joining this realm using a credential cache is not supported"
msgstr ""
"Η σύνδεση αυτού του πεδίου χρησιμοποιώντας μια κρυφή μνήμη διαπιστευτηρίου "
"δεν υποστηρίζεται"

#: service/realm-kerberos.c:278
msgid "Leaving this realm using a credential cache is not supported"
msgstr ""
"Η απομάκρυνση αυτού του τομέα χρησιμοποιώντας μια κρυφή μνήμη "
"διαπιστευτηρίου δεν υποστηρίζεται"

#: service/realm-kerberos.c:281
msgid "Joining this realm using a secret is not supported"
msgstr ""
"Η σύνδεση αυτού του πεδίου χρησιμοποιώντας ένα μυστικό δεν υποστηρίζεται"

#: service/realm-kerberos.c:282
msgid "Unenrolling this realm using a secret is not supported"
msgstr ""
"Η αφαίρεση εγγραφής αυτού του πεδίου χρησιμοποιώντας ένα μυστικό δεν "
"υποστηρίζεται"

#: service/realm-kerberos.c:285
msgid "Enrolling this realm using a password is not supported"
msgstr ""
"Η εγγραφή αυτού του πεδίου χρησιμοποιώντας έναν κωδικό πρόσβασης δεν "
"υποστηρίζεται"

#: service/realm-kerberos.c:286
msgid "Unenrolling this realm using a password is not supported"
msgstr ""
"Η αφαίρεση εγγραφής αυτού του πεδίου χρησιμοποιώντας έναν κωδικό πρόσβασης "
"δεν υποστηρίζεται"

#: service/realm-kerberos.c:312
msgid "Joining this realm is not supported"
msgstr "Η σύνδεση αυτού του πεδίου δεν υποστηρίζεται"

#: service/realm-kerberos.c:313
msgid "Leaving this realm is not supported"
msgstr "Η απομάκρυνση αυτού του πεδίου δεν υποστηρίζεται"

#: service/realm-kerberos.c:333 service/realm-kerberos.c:506
msgid "Already running another action"
msgstr "Εκτελείται ήδη μια άλλη ενέργεια"

#: service/realm-kerberos.c:377
#, c-format
msgid "Already joined to another domain: %s"
msgstr "Έχετε ήδη συνδεθεί σε έναν άλλο τομέα: %s"

#: service/realm-kerberos.c:448
msgid "Failed to change permitted logins. See diagnostics."
msgstr "Αποτυχία αλλαγής των επιτρεπόμενων συνδέσεων. Δείτε τα διαγνωστικά."

#: service/realm-kerberos.c:749
msgid "The realm does not allow specifying logins"
msgstr "Το πεδίο δεν επιτρέπει ορισμό συνδέσεων"

#: service/realm-kerberos.c:757
#, c-format
msgid "Invalid login argument%s%s%s does not match the login format."
msgstr "Άκυρο όρισμα σύνδεσης %s%s%s δεν ταιριάζει με τη μορφή σύνδεσης."

#: service/realm-packages.c:390
#, c-format
msgid "The following packages are not available for installation: %s"
msgstr "Τα παρακάτω πακέτα δεν είναι διαθέσιμα για εγκατάσταση: %s"

#: service/realm-packages.c:472 service/realm-packages.c:507
#, c-format
msgid "Necessary packages are not installed: %s"
msgstr "Τα απαραίτητα πακέτα δεν είναι εγκατεστημένα: %s"

#.
#. * Various people have been worried by installing packages
#. * quietly, so notify about what's going on.
#. *
#. * In reality *configuring* and *starting* a daemon is far
#. * more worrisome than the installation. It's realmd's job
#. * to configure, enable and start stuff. So if you're properly
#. * worried, remove realmd and do stuff manually.
#.
#: service/realm-packages.c:478 tools/realm-client.c:143
msgid "Installing necessary packages"
msgstr "Εγκαθίστανται τα απαραίτητα πακέτα"

#: service/realm-provider.c:163
msgid "Failed to discover realm. See diagnostics."
msgstr "Αποτυχία ανακάλυψης του πεδίου. Δείτε τα διαγνωστικά."

#: service/realm-samba.c:520
msgid "Not joined to this domain"
msgstr "Δεν συνδεθήκατε σε αυτόν τον τομέα"

#: service/realm-samba.c:530 service/realm-samba.c:572
msgid "The Samba provider cannot restrict permitted logins."
msgstr "Ο πάροχος Samba δεν μπορεί να περιορίσει τις επιτρεπόμενες συνδέσεις."

#: service/realm-sssd.c:130
#, c-format
msgid "Invalid login argument '%s' contains unsupported characters."
msgstr "Το άκυρο όρισμα σύνδεσης '%s' περιέχει μη υποστηριζόμενους χαρακτήρες."

#: service/realm-sssd-ad.c:128
msgid "Enabling SSSD in nsswitch.conf and PAM failed."
msgstr "Αποτυχία ενεργοποίησης του SSSD στα nsswitch.conf και PAM."

#: service/realm-sssd-ad.c:277
msgid "Unable to automatically join the domain"
msgstr "Αδύνατη η αυτόματη σύνδεση στον τομέα"

#: service/realm-sssd-ad.c:379
#, c-format
msgid ""
"Joining a domain with a one time password is only supported with the '%s' "
"membership software"
msgstr ""
"Η σύνδεση σε έναν τομέα με κωδικό πρόσβασης μιας φοράς υποστηρίζεται μόνο με "
"το λογισμικό μέλους '%s'"

#: service/realm-sssd-ad.c:393
#, c-format
msgid ""
"Joining a domain with a user password is only supported with the '%s' "
"membership software"
msgstr ""
"Η σύνδεση σε έναν τομέα με κωδικό πρόσβασης χρήστη υποστηρίζεται μόνο με το "
"λογισμικό μέλους '%s'"

#: service/realm-sssd-ad.c:409
msgid "Unsupported credentials for joining a domain"
msgstr "Ανυποστήρικτα διαπιστευτήρια για σύνδεση σε έναν τομέα"

#: service/realm-sssd-ad.c:465 service/realm-sssd-ipa.c:308
#: tools/realm-join.c:242
msgid "Already joined to this domain"
msgstr "Έχετε ήδη συνδεθεί με αυτόν τον τομέα"

#: service/realm-sssd-ad.c:471 service/realm-sssd-ipa.c:312
msgid "A domain with this name is already configured"
msgstr "Ένας τομέας με αυτό το όνομα έχει ήδη ρυθμιστεί"

#: service/realm-sssd-config.c:149
#, c-format
msgid "Already have domain %s in sssd.conf config file"
msgstr "Έχετε ήδη τον τομέα %s στο αρχείο ρυθμίσεων sssd.conf"

#: service/realm-sssd-config.c:198
#, c-format
msgid "Don't have domain %s in sssd.conf config file"
msgstr "Δεν έχετε τον τομέα %s στο αρχείο ρυθμίσεων sssd.conf"

#: service/realm-sssd-ipa.c:294
msgid "The computer-ou argument is not supported when joining an IPA domain."
msgstr ""
"Το όρισμα οργανωτικής μονάδας υπολογιστή δεν υποστηρίζεται κατά τη σύνδεση "
"σε τομέα IPA."

#: service/realm-sssd-ipa.c:298
#, fuzzy
msgid ""
"The do-not-touch-config option is not supported when joining an IPA domain."
msgstr ""
"Το όρισμα οργανωτικής μονάδας υπολογιστή δεν υποστηρίζεται κατά τη σύνδεση "
"σε τομέα IPA."

#: service/realm-sssd-ipa.c:466
msgid "Not currently joined to this realm"
msgstr "Δεν είσαστε συνδεμένοι τώρα σε αυτό το πεδίο"

#: tools/realm.c:40
msgid "Discover available realm"
msgstr "Ανακαλύψτε το διαθέσιμο πεδίο"

#: tools/realm.c:41
msgid "Enroll this machine in a realm"
msgstr "Εγγραφή αυτού του μηχανήματος σε ένα πεδίο"

#: tools/realm.c:42
msgid "Unenroll this machine from a realm"
msgstr "Αφαίρεση εγγραφής αυτού του μηχανήματος από ένα πεδίο"

#: tools/realm.c:43
msgid "List known realms"
msgstr "Να αναφέρονται τα γνωστά πεδία"

#: tools/realm.c:44
msgid "Permit user logins"
msgstr "Να επιτρέπονται οι συνδέσεις χρήστη"

#: tools/realm.c:45
msgid "Deny user logins"
msgstr "Απόρριψη συνδέσεων χρήστη"

#: tools/realm.c:182
#, c-format
msgid "Invalid value for %s option: %s"
msgstr ""

#: tools/realm.c:214
msgid "Install mode to a specific prefix"
msgstr "Εγκατάσταση κατάστασης σε συγκεκριμένο πρόθεμα"

#: tools/realm.c:215
msgid "Verbose output"
msgstr "Αναλυτική έξοδος"

#: tools/realm.c:216
msgid "Do not prompt for input"
msgstr "Να μην ζητείται είσοδος"

#: tools/realm-client.c:193 tools/realm-client.c:203
msgid "Couldn't connect to realm service"
msgstr "Αδύνατη η σύνδεση με την υπηρεσία πεδίου"

#: tools/realm-client.c:232
msgid "Couldn't load the realm service"
msgstr "Αδύνατη η φόρτωση της υπηρεσίας πεδίου"

#: tools/realm-client.c:251
msgid "Couldn't connect to system bus"
msgstr "Αδύνατη η σύνδεση με τον δίαυλο συστήματος"

#: tools/realm-client.c:281
#, c-format
msgid "Couldn't create socket pair: %s"
msgstr "Αδύνατη η δημιουργία ζεύγους υποδοχής: %s"

#: tools/realm-client.c:289 tools/realm-client.c:321
msgid "Couldn't create socket"
msgstr "Αδύνατη η δημιουργία υποδοχής"

#: tools/realm-client.c:302
msgid "Couldn't run realmd"
msgstr "Αδύνατη η εκτέλεση του realmd"

#: tools/realm-client.c:569
#, c-format
msgid "Couldn't create runtime directory: %s: %s"
msgstr "Αδύνατη η δημιουργία καταλόγου χρόνου εκτέλεσης: %s: %s"

#: tools/realm-client.c:579
#, c-format
msgid "Couldn't create credential cache file: %s: %s"
msgstr "Αδύνατη η δημιουργία αρχείου κρυφής μνήμης διαπιστευτηρίου: %s: %s"

#: tools/realm-client.c:589
msgid "Couldn't resolve credential cache"
msgstr "Αδύνατη η εύρεση κρυφής μνήμης διαπιστευτηρίου"

#: tools/realm-client.c:679
#, c-format
msgid "Invalid password for %s"
msgstr "Άκυρος κωδικός πρόσβασης για το %s"

#: tools/realm-client.c:683
#, c-format
msgid "Couldn't authenticate as %s"
msgstr "Αδύνατη η πιστοποίηση ως %s"

#: tools/realm-client.c:708
#, c-format
msgid "Couldn't parse user name: %s"
msgstr "Αδύνατη η ανάλυση ονόματος χρήστη: %s"

#: tools/realm-client.c:732
msgid "Couldn't read credential cache"
msgstr "Αδύνατη η ανάγνωση κρυφής μνήμης διαπιστευτηρίου"

#: tools/realm-client.c:758 tools/realm-client.c:965
msgid "Couldn't initialize kerberos"
msgstr "Αδύνατη η αρχικοποίηση του kerberos"

#: tools/realm-client.c:821
msgid "Cannot prompt for a password when running in unattended mode"
msgstr ""
"Αδύνατη η αίτηση για κωδικό πρόσβασης όταν εκτελείται σε αφύλακτη κατάσταση"

#: tools/realm-client.c:825
#, c-format
msgid "Password for %s: "
msgstr "Κωδικός πρόσβασης για το %s: "

#: tools/realm-client.c:843
#, c-format
msgid "Couldn't prompt for password: %s"
msgstr "Αδύνατη η αίτηση για κωδικό πρόσβασης: %s"

#: tools/realm-client.c:878
msgid "Realm does not support membership using a password"
msgstr "Το πεδίο δεν υποστηρίζει μέλος χρησιμοποιώντας έναν κωδικό πρόσβασης"

#: tools/realm-client.c:917
msgid "Realm does not support membership using a one time password"
msgstr ""
"Το πεδίο δεν υποστηρίζει μέλος χρησιμοποιώντας έναν κωδικό πρόσβασης μιας "
"φοράς"

#: tools/realm-client.c:985
msgid "Couldn't select kerberos credentials"
msgstr "Αδύνατη η επιλογή διαπιστευτηρίων του kerberos"

#: tools/realm-client.c:999
msgid "Couldn't read kerberos credentials"
msgstr "Αδύνατη η ανάγνωση διαπιστευτηρίων του kerberos"

#: tools/realm-client.c:1049
msgid "Realm does not support automatic membership"
msgstr "Το πεδίο δεν υποστηρίζει αυτόματα μέλη"

#: tools/realm-discover.c:136
msgid "Couldn't discover realms"
msgstr "Αδύνατη η ανακάλυψη πεδίων"

#: tools/realm-discover.c:156
msgid "No default realm discovered"
msgstr "Δεν βρέθηκε προεπιλεγμένο πεδίο"

#: tools/realm-discover.c:158
#, c-format
msgid "No such realm found: %s"
msgstr "Δεν βρέθηκε τέτοιο πεδίο: %s"

#: tools/realm-discover.c:183
msgid "Show all discovered realms"
msgstr "Να εμφανίζονται όλα τα πεδία που βρίσκονται"

#: tools/realm-discover.c:184 tools/realm-discover.c:278
msgid "Show only the names"
msgstr "Να εμφανίζονται μόνο τα ονόματα"

#: tools/realm-discover.c:185 tools/realm-join.c:296 tools/realm-leave.c:271
msgid "Use specific client software"
msgstr "Χρήση ιδιαίτερου λογισμικού πελάτη"

#: tools/realm-discover.c:186 tools/realm-join.c:302
msgid "Use specific membership software"
msgstr "Χρήση ιδιαίτερου λογισμικού μέλους"

#: tools/realm-discover.c:187 tools/realm-join.c:312 tools/realm-leave.c:274
msgid "Use specific server software"
msgstr "Χρήση ιδιαίτερου λογισμικού διακομιστή"

#: tools/realm-discover.c:188 tools/realm-join.c:318 tools/realm-leave.c:276
msgid "Use ldaps to connect to LDAP"
msgstr ""

#: tools/realm-discover.c:277
msgid "Show all realms"
msgstr "Να εμφανίζονται όλα τα πεδία"

#: tools/realm-join.c:92 tools/realm-join.c:132 tools/realm-join.c:163
msgid "Couldn't join realm"
msgstr "Αδύνατη η σύνδεση με το πεδίο"

#: tools/realm-join.c:233
msgid "Cannot join this realm"
msgstr "Αδύνατη η σύνδεση με αυτό το πεδίο"

#: tools/realm-join.c:235
msgid "No such realm found"
msgstr "Δεν βρέθηκε τέτοιο πεδίο"

#: tools/realm-join.c:294
msgid "Turn off automatic id mapping"
msgstr ""

#: tools/realm-join.c:298
#, fuzzy
msgid "Use specific computer name instead of hostname"
msgstr "Χρήση ιδιαίτερου λογισμικού πελάτη"

#: tools/realm-join.c:300
msgid "Computer OU DN to join"
msgstr "DN OU υπολογιστή για σύνδεση"

#: tools/realm-join.c:304
msgid "Join automatically without a password"
msgstr "Αυτόματη σύνδεση χωρίς κωδικό πρόσβασης"

#: tools/realm-join.c:306
msgid "Join using a preset one time password"
msgstr ""
"Σύνδεση χρησιμοποιώντας έναν προκαθορισμένο κωδικό πρόσβασης για μια φορά"

#: tools/realm-join.c:308
#, fuzzy
msgid "Use specific operation system name"
msgstr "Χρήση ιδιαίτερου λογισμικού πελάτη"

#: tools/realm-join.c:310
#, fuzzy
msgid "Use specific operation system version"
msgstr "Χρήση ιδιαίτερου λογισμικού διακομιστή"

#: tools/realm-join.c:314
msgid "User name to use for enrollment"
msgstr "Όνομα χρήστη για χρήση στην εγγραφή"

#: tools/realm-join.c:316
msgid "Set the user principal for the computer account"
msgstr "Ορισμός του κύριου χρήστη για τον λογαριασμό του υπολογιστή"

#: tools/realm-join.c:320
msgid "Do not change client configuration"
msgstr ""

#: tools/realm-join.c:340
msgid "Specify one realm to join"
msgstr "Ορισμός ενός πεδίου για σύνδεση"

#: tools/realm-join.c:345
msgid ""
"The --no-password argument cannot be used with --one-time-password or --user"
msgstr ""
"Το όρισμα --no-password δεν μπορεί να χρησιμοποιηθεί με --one-time-password "
"ή --user"

#: tools/realm-join.c:350
msgid "The --one-time-password argument cannot be used with --user"
msgstr "Το όρισμα --one-time-password δεν μπορεί να χρησιμοποιηθεί με --user"

#: tools/realm-leave.c:181 tools/realm-leave.c:210
msgid "Couldn't leave realm"
msgstr "Αδύνατη η αποχώρηση από το πεδίο"

#: tools/realm-leave.c:272
msgid "Remove computer from realm"
msgstr "Αφαίρεση του υπολογιστή από το πεδίο"

#: tools/realm-leave.c:275
msgid "User name to use for removal"
msgstr "Το όνομα χρήστη που θα χρησιμοποιηθεί για αφαίρεση"

#: tools/realm-logins.c:129 tools/realm-logins.c:175
msgid "Couldn't change permitted logins"
msgstr "Αδύνατη η αλλαγή των επιτρεπόμενων συνδέσεων"

#: tools/realm-logins.c:200
msgid "Permit any realm account login"
msgstr "Να επιτρέπεται η σύνδεση λογαριασμού οποιουδήποτε πεδίου"

#: tools/realm-logins.c:200
msgid "Deny any realm account login"
msgstr "Να απαγορεύεται η σύνδεση λογαριασμού οποιουδήποτε πεδίου"

#: tools/realm-logins.c:202
msgid "Withdraw permit for a realm account to login"
msgstr "Απόσυρση άδειας για έναν λογαριασμό πεδίου προς σύνδεση"

#: tools/realm-logins.c:204
msgid "Treat names as groups which to permit"
msgstr "Να θεωρούνται τα ονόματα ως ομάδες που επιτρέπουν"

#: tools/realm-logins.c:205
msgid "Realm to permit/deny logins for"
msgstr "Πεδίο για το οποίο επιτρέπονται/απαγορεύονται οι συνδέσεις"

#: tools/realm-logins.c:219
msgid "No logins should be specified with -a or --all"
msgstr "Δεν πρέπει να ορίζονται συνδέσεις με -a ή --all"

#: tools/realm-logins.c:222
msgid "The --withdraw or -x arguments cannot be used when denying logins"
msgstr ""
"Τα ορίσματα με --withdraw ή -x δεν μπορούν να χρησιμοποιηθούν κατά την "
"άρνηση συνδέσεων"

#: tools/realm-logins.c:225
msgid "Specific logins must be specified with --withdraw"
msgstr "Ειδικές συνδέσεις πρέπει να οριστούν με --withdraw"

#: tools/realm-logins.c:228
msgid "Groups may not be specified with -a or --all"
msgstr "Δεν μπορούν να οριστούν ομάδες με -a ή --all"

#: tools/realm-logins.c:235
msgid "Use --all to deny all logins"
msgstr "Χρήση του --all για άρνηση όλων των συνδέσεων"

#: tools/realm-logins.c:237
msgid "Specify specific users to add or remove from the permitted list"
msgstr ""
"Ορίστε συγκεκριμένους χρήστες που θα προστεθούν ή θα αφαιρεθούν από τον "
"επιτρεπόμενο κατάλογο"

#: tools/realm-logins.c:241
msgid ""
"Specifying deny without --all is deprecated. Use realm permit --withdraw"
msgstr ""
"Ο ορισμός άρνησης χωρίς --all είναι παρωχημένος. Χρησιμοποιήστε άδεια πεδίου "
"--withdraw "
